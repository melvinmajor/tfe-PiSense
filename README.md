> # TFE - PiSense
>
> Graduation work, 3rd Bachelor at EPHEC Louvain-la-Neuve

<img src="doc\PiSense-logo\PiSense_logo.svg" alt="PiSense logo" width="150px" style="display: block; margin: 0 auto;">

## The project

As part of a business request by Dreamnet SPRL, an IoT system that tracks temperature, humidity and CO2 in a room is required to meet a future project to new needs.

Based on first feedback with Professor A. Dewulf (EPHEC), there is also interest in detecting Silicon Oxide from a client with a career.
This will allow the customer to prevent any risk related to the presence of Silicon Oxide at a rate too high.

### Goal

Allow the optimization of the staff environment.
Studies have shown that a room with good room temperature and good air quality can help concentration and improve the health of the human being.

In the case of the professor's client returns, this would avoid a potential Silicon Oxide danger in a risky workplace (career).

## License

This project is currently not licensed but is subject to change during the development process.
It's made available on GitHub for "documentation" purpose.
If you want to distribute it and/or use it commercially, please reach me.

## Built With and For

![Python Version](https://img.shields.io/badge/Python-3.8+-informational?style=for-the-badge&labelColor=757575&color=78909c&logo=python&logoColor=white)

![Raspberry Pi devices](https://img.shields.io/badge/Raspberry-Pi_devices-informational?style=for-the-badge&color=c51a4a&logo=raspberry-pi&logoColor=white)

![OVH VPS](https://img.shields.io/badge/OVH-VPS-informational?style=for-the-badge&color=123f6d&logo=ovh&logoColor=white) ![Operating system version](https://img.shields.io/badge/Ubuntu-19.10_LTS-informational?style=for-the-badge&color=e95420&logo=ubuntu&logoColor=white)

* [PyCharm Community Edition](https://www.jetbrains.com/pycharm/) - A great Python IDE
